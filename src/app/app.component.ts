import { Component, ViewChild } from '@angular/core';
import { Nav, Platform, ModalController, AlertController } from 'ionic-angular';
import { StatusBar } from '@ionic-native/status-bar';
import { SplashScreen } from '@ionic-native/splash-screen';
import { Events } from 'ionic-angular';
import { OneSignal } from '@ionic-native/onesignal';

import { ProfilePage } from '../pages/login/profile/profile';
import { HistoryPage } from '../pages/history/history/history';
import { AuthService } from '../services/auth';
import { HelpPage } from '../pages/help/help';
import { NaytiPage } from '../pages/pogruzki/nayti/nayti';
import { ClearService } from '../services/clear';
import { MainTabs } from '../pages/moiPogruzki/mainTab';
import { Diagnostic } from '@ionic-native/diagnostic';
import { Network } from '@ionic-native/network';
import { LoginPage } from '../pages/login/login/login';
import { ConfigService } from '../services/config';


@Component({
  templateUrl: 'app.html'
})

export class MyApp {
  @ViewChild(Nav) nav: Nav;
  
    rootPage: any;
    activePage: any;
    isUserLogedIn: boolean=false;
    isLogedOut: boolean = false;

    pages: Array<{title: string, component: any, iosIcon: string, andrIcon: string,}>;
  
    constructor(public platform: Platform,
                public statusBar: StatusBar,
                public splashScreen: SplashScreen,
                private authS: AuthService,
                private clearS: ClearService,
                public modalCtrl: ModalController,
                private network: Network,
                private alertCtrl: AlertController,
                private diagnostic: Diagnostic,
                private configS: ConfigService,
                public events: Events) {

      this.initialApp();
                  
      let userToken = JSON.parse(localStorage.getItem("gte"));
      console.log(userToken);
      if(userToken && userToken != null){
        this.authS.isUserLogedIn = true;
      }


      this.events.subscribe('user:login', () => {
        this.menuLists();
      });
    }
  

    menuLists(){

      this.isUserLogedIn = this.authS.isUserLogedIn;
      console.log("this.isUserLogedIn = " + this.isUserLogedIn);
      let userToken = JSON.parse(localStorage.getItem("gte"));
        if(!userToken && userToken == null){
          this.pages = [
            { title: 'Найти погрузки', component: NaytiPage, iosIcon: "ios-search", andrIcon:"md-search"},
            { title: 'Войти',          component: LoginPage, iosIcon:"ios-log-in",  andrIcon:"md-log-in" }
          ];
          if(!this.isLogedOut){
            console.log("this.isLogedOut = " + this.isLogedOut);
            this.rootPage = NaytiPage;
            this.activePage = this.pages[0];
          }else{
            console.log("this.isLogedOut = " + this.isLogedOut);
            this.nav.setRoot(LoginPage);
            this.activePage = this.pages[1];
          }
        }else{
          this.pages = [
            { title: 'Мой профиль',    component: ProfilePage,     iosIcon: "ios-person-outline",      andrIcon:"ios-person-outline"},
            { title: 'Найти погрузки', component: NaytiPage,       iosIcon: "ios-search",              andrIcon:"md-search"},
            { title: 'Мои погрузки',   component: MainTabs,        iosIcon: "ios-list",                andrIcon:"md-list"},
            { title: 'История',        component: HistoryPage,     iosIcon: "ios-folder-open-outline", andrIcon:"md-folder-open"},
            { title: 'Помощь',         component: HelpPage,        iosIcon: "ios-help-circle-outline", andrIcon:"ios-help-circle-outline"}
          ];
          this.activePage = this.pages[1];
          this.rootPage = NaytiPage;
        }
    }


    initialApp() {
      this.platform.ready().then(() => {
        this.statusBar.styleDefault();        
          this.checkGPS();

          this.menuLists();
          
          this.checkActive(this.pages[0]);

          this.checkInternetConnection();    
      });
    }


    openPage(page) {
      this.nav.setRoot(page.component);
      this.activePage = page;
    }

    checkActive(page){
      return page == this.activePage;
    }

    checkInternetConnection(){
      this.network.onDisconnect().subscribe(() => {
        console.log('network was disconnected :-(');
        this.showNetworkAlert();
      });
  
      this.network.onConnect().subscribe(res=>{
        console.log('network was connected :-)');
      });
    }


    showNetworkAlert() {    
      let networkAlert = this.alertCtrl.create({
        title: 'Нет соединение с интернетом!',
        message: 'Пожалуйста проверьте соединение к интернету.',
        buttons: [
          {
            text: 'Отмена',
            handler: () => {
            }
          },
          {
            text: 'Настройки',
            handler: () => {
              networkAlert.dismiss().then(() => {
                  this.showSettings();
              })
            }
          }
        ]
      });
      networkAlert.present();
    }


    private showSettings() {
      this.diagnostic.switchToWifiSettings();
    };

    checkGPS(){
      this.diagnostic.isLocationEnabled().then((res)=>{
        if(!res){
          this.gpsAelrt();
        }
      }, (error) =>{
      })
    }

    gpsAelrt() {
      let networkAlert = this.alertCtrl.create({
        title: 'Внимание!',
        message: 'GPS-модуль отключен. Пожалуйста, включите GPS',
        buttons: [
          {
            text: 'OK',
            handler: () => {
            }
          }
        ]
      });
      networkAlert.present();
    }
    
    onLogout(){
        localStorage.removeItem("gte");
      
        this.authS.isUserLogedIn = false;
        this.authS.fromPogruzka = false
        this.isLogedOut = true;
        this.authS.closeModal = false;
        this.authS.userIdForPush = "";
        this.menuLists();
        this.clearS.clearVar();
    }

}
