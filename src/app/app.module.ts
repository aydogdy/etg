import { BrowserModule } from '@angular/platform-browser';
import { ErrorHandler, NgModule } from '@angular/core';
import { IonicApp, IonicErrorHandler, IonicModule } from 'ionic-angular';
import { HttpModule } from         '@angular/http';
import { LocalNotifications } from '@ionic-native/local-notifications';
import { OneSignal } from          '@ionic-native/onesignal';
import { SentryErrorHandler } from '../services/sentry-errorhandler';

// NativePligins
import { DatePicker } from      '@ionic-native/date-picker';
import { Camera } from          '@ionic-native/camera';
import { PhotoViewer } from     '@ionic-native/photo-viewer';
import { BackgroundGeolocation } from '@ionic-native/background-geolocation';
import { Geolocation } from    '@ionic-native/geolocation';
import { DatePipe } from       '@angular/common';
import { NativeGeocoder } from '@ionic-native/native-geocoder';
import { Network } from '@ionic-native/network';

import { MyApp } from './app.component';
import { StatusBar } from       '@ionic-native/status-bar';
import { SplashScreen } from    '@ionic-native/splash-screen';
import { LaunchNavigator } from '@ionic-native/launch-navigator';

//Modules
import { CarSelectionPageModule } from  '../pages/pogruzki/car-selection/car-selection.module';
import { NaytiPageModule } from         '../pages/pogruzki/nayti/nayti.module';
import { PogruzkaPageModule } from      '../pages/pogruzki/pogruzka/pogruzka.module';
import { PogruzkiPageModule } from      '../pages/pogruzki/pogruzki/pogruzki.module';
import { SearchAddressPageModule } from '../pages/pogruzki/search-address/search-address.module';

import { RegFormPageModule } from       '../pages/login/reg-form/reg-form.module';
import { LoginPage } from               '../pages/login/login/login';
import { AutoPageModule } from          '../pages/login/auto/auto.module';
import { SplashPage } from              '../pages/splash/splash';
import { ProfilePageModule } from       '../pages/login/profile/profile.module';
import { MoiPogruzkiPageModule } from   '../pages/moiPogruzki//moi-pogruzki/moi-pogruzki.module';
import { PogruzkaTabs } from            '../pages/moiPogruzki/pogruzkaTab';
import { InfoPageModule } from          '../pages/moiPogruzki/info/info.module';
import { MapPageModule } from           '../pages/moiPogruzki/map/map.module';
import { HelpPageModule } from          '../pages/help/help.module';

import { AuthService } from        '../services/auth';
import { SmsPageModule } from      '../pages/login/sms/sms.module';
import { PogruzkiService } from    '../services/pogruzki';
import { ProfileService } from     '../services/profile';
import { ClearService } from       '../services/clear';
import { MainTabs } from           '../pages/moiPogruzki/mainTab';
import { HistoryService } from     '../services/history';
import { HistoryPageModule } from  '../pages/history/history/history.module';
import { ModalPageModule } from    '../pages/history/modal/modal.module';
import { ConfigService } from      '../services/config';
import { LocationTracker } from    '../providers/location-tracker';
import { Diagnostic } from '@ionic-native/diagnostic';

import { TextMaskModule } from 'angular2-text-mask';

@NgModule({
  declarations: [
    MyApp,
    LoginPage,
    SplashPage,
    PogruzkaTabs,
    MainTabs
  ],
  imports: [
    BrowserModule,
    CarSelectionPageModule,
    NaytiPageModule,
    PogruzkaPageModule,
    PogruzkiPageModule,
    SearchAddressPageModule,
    RegFormPageModule,
    ProfilePageModule,  
    MoiPogruzkiPageModule,
    MapPageModule,
    InfoPageModule,
    HistoryPageModule,
    HelpPageModule,
    AutoPageModule,
    ModalPageModule,
    SmsPageModule,
    HttpModule,
    IonicModule.forRoot(MyApp,{}),
    TextMaskModule
  ],
  bootstrap: [IonicApp],
  entryComponents: [
    MyApp,
    LoginPage,
    SplashPage,
    PogruzkaTabs,
    MainTabs
  ],
  providers: [
    StatusBar,
    SplashScreen,
    DatePicker,
    AuthService,
    PogruzkiService,
    HistoryService,
    ProfileService,
    ClearService,
    ConfigService,
    DatePipe,
    Camera,
    PhotoViewer,
    LaunchNavigator,
    LocalNotifications,
    BackgroundGeolocation,
    Geolocation,
    LocationTracker,
    NativeGeocoder,
    Diagnostic,
    Network,
    OneSignal,
    SentryErrorHandler,
    {provide: ErrorHandler, useClass: SentryErrorHandler} //IonicErrorHandler
  ]
})
export class AppModule {}
