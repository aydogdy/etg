import { Injectable } from "@angular/core";
import { Http } from "@angular/http";
import { ToastController } from 'ionic-angular';

import 'rxjs/Rx';
import { RegCreds } from "../models/auth/regCreds";
import { Auto } from "../models/auth/autoType";
import { ConfigService } from "./config";

@Injectable()
export class AuthService {

    public isUserLogedIn: boolean = false;
    autoArray: Array<Auto>;
    isLoading: boolean = false;
    isNewUser: boolean = false;

    public fromPogruzka: boolean = false;
    public closeModal: boolean = false;

    userIdForPush: string;

	constructor(private http: Http,
                private toastCtrl: ToastController,
                private configS: ConfigService){
        
    }

    presentToast(message: string, classType: string, position:string, showCloseButton:boolean, closeButtonText: string, time: number) {
        let toast = this.toastCtrl.create({
          message: message,
          position: position,
          showCloseButton: showCloseButton,
          closeButtonText: closeButtonText,
          cssClass: classType,
          duration: time,
          dismissOnPageChange: false
        });
        toast.present();
    }

    // Список типов ТС
    getArrayTs(){
        this.isLoading = true;
        let promise = new Promise((resolve, reject) => {
            let apiURL = `${this.configS.apiRoot}/manual/type-auto`;
            this.http.get(apiURL,this.configS.options)
            .toPromise()
            .then((res) => {
                    this.isLoading = false;
                    this.autoArray = res.json().data.items;
                    resolve(res.json());
                },(msg) => {
                    this.isLoading = false;
                    reject(msg.json());
                }
            );
        });
        return promise;
    }

    getArrayCompany(inn: string){
        this.isLoading = true;
        let promise = new Promise((resolve, reject) => {
            let apiURL = `${this.configS.apiRoot}/taxi-list?inn=` + inn;
            this.http.get(apiURL, this.configS.options)
            .toPromise()
            .then((res) => {
                    this.isLoading = false;
                    resolve(res.json());
                },(msg) => {
                    this.isLoading = false;
                    reject(msg.json());
                }
            );
        });
        return promise;
    }


    // Запрос на регистрацию для нового пользователя
    getSmsCode(regCreds: RegCreds){
        let promise = new Promise((resolve, reject) => {
        let apiURL = `${this.configS.apiRoot}/register`;
        this.http.post(apiURL, JSON.stringify(regCreds), this.configS.options)
            .toPromise()
            .then((res) => {
                    resolve(res.json());
                },(msg) => {
                    reject(msg.json());
                }
            );
        });
        return promise;
    }


    // Подтверждение регистрации для нового пользователя
    confirmRegistration(crd:any){
        let promise = new Promise((resolve, reject) => {
        let apiURL = `${this.configS.apiRoot}/register-confirm`;
        this.http.post(apiURL, JSON.stringify(crd), this.configS.options)
            .toPromise()
            .then((res) => {
                    localStorage.setItem('gte', JSON.stringify({ token: res.json().data.access_token }));
                    this.isUserLogedIn = true;
                    resolve(res.json());
                },(msg) => {
                    reject(msg.json());
                }
            );
        });
        return promise;

    }


    // Запрос на смс кода для существуещего пользователя
    registeredLogin(phone: string){
        let promise = new Promise((resolve, reject) => {
        let apiURL = `${this.configS.apiRoot}/login?phone=` + phone;
        this.http.get(apiURL, this.configS.options)
            .toPromise()
            .then((res) => {
                    resolve(res.json());
                },(msg) => {
                    reject(msg.json());
                }
            );
        });
        return promise;
    };


    // Подтверждение регистрации для существуещего пользователя
    confirmLogedUser(crd:any){
            let promise = new Promise((resolve, reject) => {
                let apiURL = `${this.configS.apiRoot}/login`;
                this.http.post(apiURL, JSON.stringify(crd), this.configS.options)
                .toPromise()
                .then((res) => {
                        localStorage.setItem('gte', JSON.stringify({ "token": res.json().data.access_token, "isDriverNearLoad": false, "isDriverNearUnload": false }));
                        this.isUserLogedIn = true;
                        resolve(res.json());
                    },(msg) => {
                        reject(msg.json());
                    }
            );
        });
        return promise;
    }



}