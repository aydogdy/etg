import { Injectable } from "@angular/core";
import { Http } from "@angular/http";
import { ToastController, LoadingController } from 'ionic-angular';

import 'rxjs/Rx';
import { Profile } from "../models/auth/profile";
import { ConfigService } from "./config";

@Injectable()
export class ProfileService {

    isLoading: boolean = false;
    profileData: Profile;
    profileSavedData: Profile;
    gte:any;

    questions: Array<{id: number, question: string, answer: string, isOpen: boolean}>;
    

	constructor(private http: Http,
                private toastCtrl: ToastController,
                private loadingCtrl: LoadingController,
                private configS: ConfigService){
        
       this.gte =  JSON.parse(localStorage.getItem("gte"));
       if(this.gte && this.gte !== null){
           this.getProfileData(true);
           this.getFaq(this.gte.token, false);
       }
    }


    presentToast(txt: string, duration: number, position: string) {
        let toast = this.toastCtrl.create({
            message: txt,
            duration: duration,
            position: position
        });
        toast.present();
    }


    getProfileData(showLoader: boolean){
        const loading = this.loadingCtrl.create();
        if(showLoader) loading.present();
        this.gte =  JSON.parse(localStorage.getItem("gte"));
        
        let promise = new Promise((resolve, reject) => {
            let apiURL = `${this.configS.apiRoot}/user?access_token=` + this.gte.token;
            this.http.get(apiURL, this.configS.options)
            .toPromise()
            .then((res) => {
                    if(showLoader) loading.dismiss();
                    this.profileSavedData = res.json().data;
                    console.log("profileSavedData ", this.profileSavedData);
                    this.gte["nds"] = this.profileSavedData.nds;
                    localStorage.setItem('gte', JSON.stringify(this.gte));
                    resolve(res.json());
            },(msg) => {
                    if(showLoader) loading.dismiss();
                    reject(msg.json());
            });
        });
        return promise;
    };



    updateUserData(creds){
        const loading = this.loadingCtrl.create();
        loading.present();
        this.gte =  JSON.parse(localStorage.getItem("gte"));
        
        let promise = new Promise((resolve, reject) => {
            let apiURL = `${this.configS.apiRoot}/user?access_token=` + this.gte.token;
            this.http.put(apiURL, JSON.stringify(creds), this.configS.options)
            .toPromise()
            .then((res) => {
                    loading.dismiss();
                    resolve(res.json());
            },(msg) => {
                    loading.dismiss();
                    reject(msg.json());
            });
        });
        return promise;
    }

    getFaq(token: string, showLoader: boolean){
        const loading = this.loadingCtrl.create();
        if(showLoader) loading.present();
        let promise = new Promise((resolve, reject) => {
            let apiURL = `${this.configS.apiRoot}/faq?access_token=` + token;
            this.http.get(apiURL, this.configS.options)
            .toPromise()
            .then((res) => {
                    if(showLoader) loading.dismiss();
                    resolve(res.json());
                    this.questions = res.json().data.items;
                },(msg) => {
                    if(showLoader) loading.dismiss();
                    reject(msg.json());
                });
        });
        return promise;
    }
    
}