import { Injectable, ViewChild } from "@angular/core";
import { Http } from "@angular/http";
import { LoadingController, Nav } from 'ionic-angular';

import 'rxjs/Rx';
import { Histori } from "../models/pogruzki/history";
import { ConfigService } from "./config";
import { OneSignal } from "@ionic-native/onesignal";
import { MainTabs } from "../pages/moiPogruzki/mainTab";
import { AuthService } from "./auth";

@Injectable()
export class HistoryService {

    historyArray: Array<Histori>=[];
    @ViewChild(Nav) nav: Nav;

	constructor(private http: Http,
                private loadingCtrl: LoadingController,
                private oneSignal: OneSignal,
                private authS: AuthService,
                private configS: ConfigService){
    }

    
    getMyHistory(showLoader: boolean){
        let gte = JSON.parse(localStorage.getItem("gte"));
        const loading = this.loadingCtrl.create();
        if(showLoader) loading.present();

        let promise = new Promise((resolve, reject) => {
            let apiURL = `${this.configS.apiRoot}/order/history?access_token=` + gte.token;
            this.http.get(apiURL, this.configS.options)
            .toPromise()
            .then((res) => {
                    if(showLoader) loading.dismiss();
                    this.historyArray = res.json().data.items;
                    console.log(this.historyArray);
                    resolve(res.json());
                },(msg) => {
                    if(showLoader) loading.dismiss();
                    reject(msg.json());
                });
        });
        return promise;
    }


    getHistoryStates(id: number){
        let gte = JSON.parse(localStorage.getItem("gte"));
        let promise = new Promise((resolve, reject) => {
            let apiURL = `${this.configS.apiRoot}/order/`+id+'/state?access_token=' + gte.token;
            this.http.get(apiURL, this.configS.options)
            .toPromise()
            .then((res) => {
                    resolve(res.json());
                },(msg) => {
                    reject(msg.json());
                });
        });
        return promise;
    }



    oneSignalConf(){
        this.oneSignal.startInit('e9613918-23f4-471b-a357-524ea0152bbd', '316836843621'); //OneSignalAppId (Keys & IDs), Firebase-Идентификатор отправителя(Cloud messaging)
                                  
        this.oneSignal.inFocusDisplaying(this.oneSignal.OSInFocusDisplayOption.Notification);
  
        this.oneSignal.handleNotificationReceived().subscribe(() => {
        });
        
        this.oneSignal.handleNotificationOpened().subscribe(() => { // when Push notification tepped, will go to MainPage 
          this.nav.setRoot(MainTabs);
        });
  
        this.oneSignal.endInit();
  
        this.oneSignal.getIds().then((ids) =>{ // user id for sending PUSH
          this.authS.userIdForPush = ids.userId;
        });
    }
    

}