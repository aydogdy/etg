import { Injectable } from "@angular/core";
import { Http } from "@angular/http";
import { ToastController, LoadingController, App } from 'ionic-angular';

import 'rxjs/Rx';
import { Pogruzka } from "../models/pogruzki/pogruzka";
import { PogruzkaList } from "../models/pogruzki/pogruzkaList";
import { ConfigService } from "./config";
import { LocalNotifications } from "@ionic-native/local-notifications";

@Injectable()
export class PogruzkiService {

    isLoading: boolean = false;
    isObratnyePogruzkiPage: boolean = true;
    showBackOrders: boolean = false;
    pogruzkiArray: Array<PogruzkaList>=[];
    myPogruzkiArray: Array<Pogruzka> = [];
    selectedPogruzka: Pogruzka;
    updateTime: Number; 
    gte: any;
    selectedPogruzkaIndx: number = null;
    coordinatesInterval: number = 10000;

    isDriverNearPoint: boolean = false;
    activeOrderId: number = null;

    distpatcherAcceptedOrders: Array<{id: number, state: boolean}>=[];

	constructor(private http: Http,
                private toastCtrl: ToastController,
                public app: App,
                private localNotifications: LocalNotifications,
                private configS: ConfigService,
                private loadingCtrl: LoadingController){
        
       this.gte = JSON.parse(localStorage.getItem("gte"));
       if(this.gte && this.gte !== 'null'){
          this.longPoll(0);
        };

        
       console.log("************PogruzkiService*********");
    }

    closeModal(){
        this.app.goBack();
    }
    
    presentToast(message: string, classType: string, position:string, showCloseButton:boolean, closeButtonText: string, time: number) {
        let toast = this.toastCtrl.create({
          message: message,
          position: position,
          showCloseButton: showCloseButton,
          closeButtonText: closeButtonText,
          cssClass: classType,
          duration: time,
          dismissOnPageChange: true
        });

        toast.present();
    }

    showLocalNotification(text: string){
        this.localNotifications.schedule({
            id: 1,
            text: text
          });
    }

// ******************************************** Найти/Поиск погрузок **************************************** //
    onGetOrders(creds: any){
        console.log(creds);
        let gte = JSON.parse(localStorage.getItem("gte"));
        let apiURL;
        console.log(creds);
        let promise = new Promise((resolve, reject) => {
           if(gte && gte !== "null"){
                console.log(gte);
                this.longPoll(0);
                apiURL = `${this.configS.apiRoot}/order?access_token=`+gte.token+"&from_lat="+creds.from_lat+"&from_lng="+creds.from_lng+"&from_radius="+creds.from_radius+"&from_note="+creds.from_note+"&to_lat="+creds.to_lat+"&to_lng="+creds.to_lng+"&to_radius="+creds.to_radius+"&date="+"&to_note="+creds.to_note+"&date"+creds.date+"&access_token=" + gte.token;
           }else{
                apiURL = `${this.configS.apiRoot}/order?from_lat=`+creds.from_lat+"&from_lng="+creds.from_lng+"&from_radius="+creds.from_radius+"&from_note="+creds.from_note+"&to_lat="+creds.to_lat+"&to_lng="+creds.to_lng+"&to_radius=" + creds.to_radius +"&to_note="+creds.to_note+"&date="+creds.date;
           }
           console.log(apiURL);
            this.http.get(apiURL, this.configS.options)
            .toPromise()
            .then((res) => {
                    this.pogruzkiArray = res.json().data.items
                    console.log("this.pogruzkiArray", this.pogruzkiArray);
                    resolve(res.json());
                },(msg) => {
                    reject(msg.json());
                }
            );
        });
        return promise;
    }

// ******************************************** Информация о найденного заказа **************************************** //
    onGetOrderInfo(id: number){
        let gte = JSON.parse(localStorage.getItem("gte"));
        let apiURL;
        let promise = new Promise((resolve, reject) => {
            if(gte && gte !== "null"){
                apiURL = `${this.configS.apiRoot}/order/`+ id + "?access_token=" + gte.token;
                
            }else{
                apiURL = `${this.configS.apiRoot}/order/`+ id;
            }
            this.http.get(apiURL, this.configS.options)
            .toPromise()
            .then((res) => {
                    this.selectedPogruzka = res.json().data
                    console.log(this.selectedPogruzka);
                    resolve(res.json());
                },(msg) => {
                    reject(msg.json());
                });
        });
        return promise;
    }

// ******************************************** Предложение цены **************************************** //
    sendUserPrice(creds){
        let gte = JSON.parse(localStorage.getItem("gte"));
        let promise = new Promise((resolve, reject) => {
        let apiURL = `${this.configS.apiRoot}/order/`+ this.selectedPogruzka.id + '/offer?access_token=' + gte.token;
        this.http.post(apiURL, JSON.stringify(creds), this.configS.options)
            .toPromise()
            .then((res) => {
                resolve(res.json());
            },(msg) => {
                reject(msg.json());
            });
        });
        return promise;
    }

// ******************************************** ЛонгПул **************************************** //
    longPoll(timer: Number){
        let gte = JSON.parse(localStorage.getItem("gte"));
        if(gte && gte!=='null'){
            console.log("************* Подписываеся longPoll *********************")
            let apiURL = `${this.configS.apiRoot}/longpool?updated_at=` + timer + "&access_token=" + gte.token;
            this.http.get(apiURL)
            .subscribe((data: any) => {
                console.log(data.json().data);
                let appoint = data.json().data.commands.appoint;
                let ready = data.json().data.commands.ready;
                
                var isNotDublicate = true;

                if(appoint){
                    let items = appoint.items;
                    for(let i=0; i<items.length; i++){

                        for(let g=0; g<this.myPogruzkiArray.length; g++){
                            if(items[i].id === this.myPogruzkiArray[g].id)
                            isNotDublicate = false;
                        }

                        if(isNotDublicate){
                            this.myPogruzkiArray.unshift(items[i]);
                            isNotDublicate = true;
                        }
                    }
                    this.showLocalNotification("Поздравляем! Вам назначен заказ.Срочно подтвердите рейс.");
                }
                if(ready){
                    let acceptedItems = ready.items;
                    for(let i=0; i<acceptedItems.length; i++){
                        let acceptedItem = acceptedItems[i];
                        console.log("Заказ с № " + acceptedItem.id + " подтвержден для выполнене!", acceptedItem);
                        this.showLocalNotification("Заказ с № " + acceptedItem.id + " подтвержден для выполнене!");
                        for(let j=0; j<this.myPogruzkiArray.length; j++){
                            if(this.myPogruzkiArray[j].id === acceptedItem.id){
                                this.myPogruzkiArray[j].status.id = acceptedItem.status.id;
                                this.activeOrderId = acceptedItem.id;
                            }
                        }
                    }
                }
                console.log(this.myPogruzkiArray);
                this.longPoll(data.json().data.last);
            },(err) => {
                this.longPoll(0);
            });
        }
    }

// ******************************************** МОИ (активные) ПОГРУЗКИ ****************************************
    getMyPoints(showLoader: boolean){
        let gte = JSON.parse(localStorage.getItem("gte"));
        const loading = this.loadingCtrl.create();
        if(showLoader) loading.present();

        let promise = new Promise((resolve, reject) => {
            let apiURL = `${this.configS.apiRoot}/order/active?access_token=` + gte.token;
            this.http.get(apiURL, this.configS.options)
            .toPromise()
            .then((res) => {
                    console.log(res);
                    if(showLoader) loading.dismiss();
                    this.myPogruzkiArray = res.json().data.items;
                    resolve(res.json());
                },(msg) => {
                    if(showLoader) loading.dismiss();
                    reject(msg.json());
                });
        });
        return promise;
    };

// ******************************************** Обрытные ПОГРУЗКИ ****************************************//
    getMyBackPoints(showLoader: boolean){
        let gte = JSON.parse(localStorage.getItem("gte"));
        const loading = this.loadingCtrl.create();
        if(showLoader) loading.present();

        let promise = new Promise((resolve, reject) => {
            let apiURL = `${this.configS.apiRoot}/order/reverse?access_token=` + gte.token;
            this.http.get(apiURL, this.configS.options)
            .toPromise()
            .then((res) => {
                    if(showLoader) loading.dismiss();
                    this.pogruzkiArray = res.json().data.items;
                    resolve(res.json());
                },(msg) => {
                    if(showLoader) loading.dismiss();
                    reject(msg.json());
                });
        });
        return promise;
    };

//***************************************** Отказаться/Отмена заказа *****************************************//
    denialOrder(id: number, indx: number){
        let gte = JSON.parse(localStorage.getItem("gte"));
        const loading = this.loadingCtrl.create();
        loading.present();

        let promise = new Promise((resolve, reject) => {
            let apiURL = `${this.configS.apiRoot}/order/` + id + '/denial?access_token=' + gte.token;
            this.http.put(apiURL, this.configS.options)
            .toPromise()
            .then((res) => {
                    loading.dismiss();
                    this.myPogruzkiArray.splice(indx, 1);
                    resolve(res.json());
                },(msg) => {
                    loading.dismiss();
                    reject(msg.json());
                });
        });
        return promise;
    };

//***************************************** Принять заказа *****************************************//
    acceptOrder(id: number){
        console.log(id);
        let gte = JSON.parse(localStorage.getItem("gte"));
        const loading = this.loadingCtrl.create();
        loading.present();

        let promise = new Promise((resolve, reject) => {
        let apiURL = `${this.configS.apiRoot}/order/` + id + '/accept?access_token=' + gte.token;
            this.http.put(apiURL, this.configS.options)
            .toPromise()
            .then((res) => {
                    loading.dismiss();
                    console.log(res.json().data);
                    this.selectedPogruzka = res.json().data;
                    this.activeOrderId = id;
                    resolve(res.json());
                },(msg) => {
                    loading.dismiss();
                    reject(msg.json());
                });
        });
        return promise;
    };


//***************************************** Смена статуса заказа *****************************************//
    stateChange(id: number, stateName: string){
        let gte = JSON.parse(localStorage.getItem("gte"));
        const loading = this.loadingCtrl.create();
        loading.present();
        let promise = new Promise((resolve, reject) => {
        let apiURL = `${this.configS.apiRoot}/order/` + id + '/'+ stateName + '?access_token=' + gte.token;;
            this.http.put(apiURL, JSON.stringify({}), this.configS.options)
            .toPromise()
            .then((res) => {
                    console.log("FINISHING************************", res.json().data.status.id);
                    loading.dismiss();
                    if(res.json().data.status.id == 9){
                        console.log("FINISHING************************");
                        this.isObratnyePogruzkiPage = true;
                        this.showBackOrders = false;
                    }
                    if(this.showBackOrders && res.json().data.status.id !== 9){
                        this.getMyBackPoints(false);
                    }
                    resolve(res.json());
                },(msg) => {
                    loading.dismiss();
                    reject(msg.json());
                });
        });
        return promise;
    };

//***************************************** Смена статуса заказа *****************************************//

    sendUserCoordinates(lat: number, lng: number){
        console.log("======ACTIVE ORDER: id:" + lat, ", lat: " + lat + ", lng: " + lng);
        let gte = JSON.parse(localStorage.getItem("gte"));
        if(gte.token && gte.token !== null){
            let promise = new Promise((resolve, reject) => {
            let apiURL = `${this.configS.apiRoot}/user/gps?` + 'access_token=' + gte.token;;
                this.http.post(apiURL, JSON.stringify({"lat": lat, "lng": lng}), this.configS.options)
                .toPromise()
                .then((res) => {
                       console.log(res.json())
                        this.coordinatesInterval = res.json().timeout;
                        resolve(res.json());
                    },(msg) => {
                        reject(msg.json());
                    });
            });
            return promise;
        }
    };

}