import { Component } from '@angular/core';
import { IonicPage, AlertController } from 'ionic-angular';
import { LaunchNavigator } from '@ionic-native/launch-navigator';
import { PogruzkiService } from '../../../services/pogruzki';


@IonicPage()
@Component({
  selector: 'page-map',
  templateUrl: 'map.html',
})
export class MapPage{

  constructor(private launchNavigator: LaunchNavigator,
              private pogruzkiS: PogruzkiService,
              private alertCtrl: AlertController) {

  }

  ionViewWillEnter(){
    this.onNavigator();
    console.log(this.pogruzkiS.selectedPogruzka);
  }

  onNavigator(){
    
          let app: any;
    
          this.launchNavigator.isAppAvailable(this.launchNavigator.APP.YANDEX_NAVIGATOR).then((data)=>{
            app = this.launchNavigator.APP.YANDEX_NAVIGATOR;
          },(error)=>{
            app = this.launchNavigator.userSelect;
          });
          
          this.launchNavigator.navigate([parseInt(this.pogruzkiS.selectedPogruzka.to.lat), parseInt(this.pogruzkiS.selectedPogruzka.to.lng)], {
              start: [parseInt(this.pogruzkiS.selectedPogruzka.from.lat), parseInt(this.pogruzkiS.selectedPogruzka.from.lng)],
              app: app
          }).then(
            success => {
              console.log('Launched navigator')
            },error => {
              let alert = this.alertCtrl.create({
                title: 'Error launching navigator!',
                subTitle: JSON.stringify(error),
                buttons: ['OK']
              });
              alert.present();
              console.log('Error launching navigator', error)
            }
          );
    
    }




}