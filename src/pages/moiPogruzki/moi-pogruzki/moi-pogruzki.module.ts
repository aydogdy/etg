import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { MoiPogruzkiPage } from './moi-pogruzki';

@NgModule({
  declarations: [
    MoiPogruzkiPage,
  ],
  imports: [
    IonicPageModule.forChild(MoiPogruzkiPage),
  ],
})
export class MoiPogruzkiPageModule {}
