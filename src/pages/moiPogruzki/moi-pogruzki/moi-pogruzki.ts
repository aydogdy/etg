import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, ModalController, App } from 'ionic-angular';
import { PogruzkaTabs } from '../pogruzkaTab';
import { PogruzkiService } from '../../../services/pogruzki';
import { Pogruzka } from '../../../models/pogruzki/pogruzka';
import { LocationTracker } from '../../../providers/location-tracker';

@IonicPage()
@Component({
  selector: 'page-moi-pogruzki',
  templateUrl: 'moi-pogruzki.html',
})
export class MoiPogruzkiPage {

  myPogruzkiArray: Array<Pogruzka>;

  constructor(public navCtrl: NavController,
              private pogruzkiS: PogruzkiService,
              private modalCtrl: ModalController,
              public navParams: NavParams,
              private app: App,
              public locationTracker: LocationTracker) {

      this.myPogruzkiArray = this.pogruzkiS.myPogruzkiArray;
      if(this.myPogruzkiArray == undefined || !this.myPogruzkiArray || this.myPogruzkiArray.length == 0){
        let showLoader = true;
        this.pogruzkiS.getMyPoints(showLoader).then(res=>{
          this.myPogruzkiArray = this.pogruzkiS.myPogruzkiArray;
        });
      }
      this.pogruzkiS.isObratnyePogruzkiPage = true;
  }

  doRefresh(refresher) {
    let showLoader = false;
    this.pogruzkiS.getMyPoints(showLoader).then(res=>{
      this.myPogruzkiArray = this.pogruzkiS.myPogruzkiArray;
      refresher.complete();
    }, error=>{
      refresher.complete();
    })
  }

  onMyPogruzka(p: Pogruzka, indx: number){
    this.pogruzkiS.isDriverNearPoint = false;
    console.log(p);
    if(p.status.id !== 1){
      if(p.status.id < 4){
        this.locationTracker.calculateUserDistance(p.from.lat, p.from.lng);
      }else{
        this.locationTracker.calculateUserDistance(p.to.lat, p.to.lng);
      }
      this.pogruzkiS.selectedPogruzka = p;
      this.pogruzkiS.selectedPogruzkaIndx = indx;
      this.app.getRootNav().setRoot(PogruzkaTabs);
    }
  }

  onDeniel(id: number, indx: number){
    this.pogruzkiS.denialOrder(id, indx);
  }

  onAccept(id: number, p:  Pogruzka, indx: number){
    this.pogruzkiS.acceptOrder(id).then((res: any)=>{
      p.status = res.data.status;
      this.onMyPogruzka(p, indx);
      this.pogruzkiS.myPogruzkiArray[indx].status = res.data.status;
      this.myPogruzkiArray = this.pogruzkiS.myPogruzkiArray;
    }, (error: any)=>{
      this.pogruzkiS.presentToast(error.message, "error", "top", false, "", 2000);
    });
  }


}
