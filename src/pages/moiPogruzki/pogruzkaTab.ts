import { Component } from '@angular/core';
import { InfoPage } from './info/info';
import { MapPage } from './map/map';
import { App, ViewController } from 'ionic-angular';
import { PogruzkiService } from '../../services/pogruzki';
import { MainTabs } from './mainTab';

@Component({
	selector: 'page-tabs',
    template: `
    
    <button ion-button clear icon-only (click)="close()" style="position: absolute;
                                                                top: 6px; left: 5px;
                                                                z-index: 999;
                                                                color: white;">
                                                            <ion-icon name="md-arrow-back"></ion-icon>
    </button>

		<ion-tabs #pogruzkaTabs  tabsPlacement='top' color='secondary'>
			<ion-tab [root]="infoPage"
			         tabTitle="Инфо"></ion-tab>
			<ion-tab [root]="mapPage"
			         tabTitle="Карта"></ion-tab>
		</ion-tabs>
	`
})
export class PogruzkaTabs {
	infoPage = InfoPage;
	mapPage = MapPage;

	constructor(public app: App,
		        private viewCtrl: ViewController,
	            private pogruzkiS: PogruzkiService){
	}
	close(){
		// this.viewCtrl.dismiss();
		this.app.getRootNav().setRoot(MainTabs);

	}


}
