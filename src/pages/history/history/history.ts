import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, ModalController } from 'ionic-angular';
import { PogruzkaList } from '../../../models/pogruzki/pogruzkaList';
import { HistoryService } from '../../../services/history';
import { ModalPage } from '../modal/modal';


@IonicPage()
@Component({
  selector: 'page-history',
  templateUrl: 'history.html',
})
export class HistoryPage {

  historyArray: Array<PogruzkaList>;

  constructor(public navCtrl: NavController,
              public navParams: NavParams,
              private modalCtrl: ModalController,
              private historyS: HistoryService) {

      this.historyArray = this.historyS.historyArray;
      this.historyS.getMyHistory(true).then((res: any)=>{
        this.historyArray = this.historyS.historyArray;
      });
      
  }

  doRefresh(refresher) {
    let showLoader = false;
    this.historyS.getMyHistory(showLoader).then(res=>{
      this.historyArray = this.historyS.historyArray;
      refresher.complete();
    })
  }

  onMoreInfo(id: number){
    this.historyS.getHistoryStates(id);
    const statesModal = this.modalCtrl.create(ModalPage, {"id": id});
    statesModal.present();
  }

}
