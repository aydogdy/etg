import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, ViewController } from 'ionic-angular';
import { HistoryService } from '../../../services/history';
import { State } from '../../../models/pogruzki/state';


@IonicPage()
@Component({
  selector: 'page-modal',
  templateUrl: 'modal.html',
})
export class ModalPage {

  isLoading: boolean = true;
  id: number;
  statesArray: Array<State>;
  
  constructor(public navCtrl: NavController,
              public navParams: NavParams,
              private viewCtrl: ViewController,
              private historyS: HistoryService) {
     this.id = this.navParams.get("id");
     this.getStates(this.id);
  }


  getStates(id: number){
    this.historyS.getHistoryStates(id).then((data: any)=>{
      this.isLoading = false;
      this.statesArray = data.data.items;
      console.log(this.statesArray);
    }, error => {
      this.isLoading = false;
    });
  }

  close(){
    this.viewCtrl.dismiss();
  }

}
