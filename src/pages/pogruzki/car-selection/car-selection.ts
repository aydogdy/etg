import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, ViewController } from 'ionic-angular';
import { AuthService } from '../../../services/auth';

@IonicPage()
@Component({
  selector: 'page-car-selection',
  templateUrl: 'car-selection.html',
})
export class CarSelectionPage {

  constructor(public navCtrl: NavController,
              public navParams: NavParams,
              private authS: AuthService,
              private viewCtlr: ViewController) {

       this.authS.autoArray=null;
       this.authS.getArrayTs();

  }

  onCloseModal(){
    this.viewCtlr.dismiss();
  }

  onSelectAuto(car: {"id": number, "name": string}){
    this.viewCtlr.dismiss({"car": car});
  }

}
