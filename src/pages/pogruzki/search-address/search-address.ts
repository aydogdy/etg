import { Component, ViewChild } from '@angular/core';
import { IonicPage, NavController, NavParams, Searchbar, ViewController } from 'ionic-angular';

@IonicPage()
@Component({
  selector: 'page-search-address',
  templateUrl: 'search-address.html',
})
export class SearchAddressPage {

  @ViewChild('searchbar') searchbar:Searchbar;
  
    searchTerm: string = '';
    isSearching: boolean = false;
    isFound: boolean = true;
    cities: Array<{id: number, name: string, lat: number, lng: number}>;

    constructor(public navCtrl: NavController,
                public navParams: NavParams,
                private viewCtlr: ViewController) {

      this.initCities(); 
                  
    }

    initCities(){
      this.cities = [{id:0, name: 'Москва', lat: 55.755826, lng: 37.617299900000035},
                     {id:1, name: 'Самара', lat: 53.2415041, lng: 50.22124629999996},
                     {id:2, name: 'Санкт-Петербург', lat: 59.934280, lng: 30.335099},
                     {id:3, name: 'Казань', lat: 55.830431, lng: 49.066081},
                     {id:4, name: 'Волгоград', lat: 48.708048, lng: 44.513304},
                     {id:5, name: 'Екатеринбург', lat: 56.838926, lng: 60.605703},
                     {id:6, name: 'Пермь', lat: 58.029681, lng: 56.266792},
                     {id:7, name: 'Уфа', lat: 54.738762, lng: 55.972055},
                     {id:8, name: 'Тюмень', lat: 57.161298, lng: 65.525017},
                     {id:9, name: 'Воронеж', lat: 51.675497, lng: 39.208882},
                     {id:10, name: 'Новосибирск', lat: 55.008353, lng: 82.935733},
                     {id:10, name: 'Тверь', lat: 56.858721, lng: 35.917596},
                     {id:10, name: 'Калугая', lat: 54.551858, lng: 36.285097},
                     {id:10, name: 'Рязань', lat: 54.609542 , lng: 39.712586},
                     {id:10, name: 'Тула', lat: 54.204836, lng: 37.618492},
                     {id:10, name: 'Владимир', lat: 56.144596, lng: 40.417869},
                     {id:10, name: 'Дзержинск', lat: 56.244099, lng: 43.435181},
                     {id:10, name: 'Ярославль', lat: 57.626074, lng: 39.884471},
                     {id:10, name: 'Брянск', lat: 53.263531, lng: 34.416110},
                     {id:10, name: 'Липецк', lat: 52.612200, lng: 39.598123},
                     {id:10, name: 'Пенза', lat: 53.227290, lng: 45.000000},
                     {id:10, name: 'Курск', lat: 51.709196, lng: 36.156224},
                     {id:10, name: 'Иваново', lat: 57.005067, lng: 40.976645},
                     {id:10, name: 'Нижний Новогород', lat: 56.296504, lng: 43.936059},
                     {id:10, name: 'Саратов', lat: 51.592365, lng: 45.960803},
                     {id:10, name: 'Чебоксары', lat: 56.116766, lng: 47.262782},
                     {id:10, name: 'Ульяновск', lat: 54.318160, lng: 48.383792},
                     {id:10, name: 'Волгоград', lat: 48.708048, lng: 44.513304},
                     {id:10, name: 'Ростов-на-Дону', lat: 47.235714, lng: 39.701505},
                     {id:10, name: 'Краснодар', lat: 45.039267, lng: 38.987221},
                     {id:10, name: 'Оренбург', lat: 51.766648, lng: 55.100454},
                     {id:10, name: 'Тольяти', lat: 53.508600, lng: 49.419834},
                     {id:10, name: 'Набережные Челны', lat: 55.718505, lng: 52.372104},
                     {id:10, name: 'Ижевск', lat: 56.861860, lng: 53.232428},
                     {id:10, name: 'Адыгейск', lat: 544.881106, lng: 39.194619}
                    ];
    };

  
    ionViewDidEnter() {
        setTimeout(() => {
            this.searchbar.setFocus();
      });
    }
  
    onCloseModal(){
      this.viewCtlr.dismiss();
    }


  
    getItems(term: any){
      this.initCities();
      this.isSearching = false;
          this.searchTerm = term.target.value;
          // if the value is an empty string don't filter the items
          if (this.searchTerm && this.searchTerm.trim() != '') {
            this.isSearching = true;
            this.cities = this.cities.filter((city) => {
              return (city.name.toLowerCase().indexOf(this.searchTerm.toLowerCase()) > -1);
            })
          }else{
            this.isSearching = false;
          }
    }


    onSelect(city: any){
      console.log(city);
      this.viewCtlr.dismiss({'city': city.name, 'lat': city.lat, 'lng': city.lng});
    }







}
