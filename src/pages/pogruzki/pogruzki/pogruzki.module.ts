import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { PogruzkiPage } from './pogruzki';

@NgModule({
  declarations: [
    PogruzkiPage,
  ],
  imports: [
    IonicPageModule.forChild(PogruzkiPage),
  ],
})
export class PogruzkiPageModule {}
