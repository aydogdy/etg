import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { NaytiPage } from './nayti';

@NgModule({
  declarations: [
    NaytiPage,
  ],
  imports: [
    IonicPageModule.forChild(NaytiPage),
  ],
})
export class NaytiPageModule {}
