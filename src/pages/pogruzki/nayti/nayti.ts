import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, ModalController, LoadingController } from 'ionic-angular';
import { Validators, FormGroup, FormBuilder } from '@angular/forms';
import { PogruzkiPage } from '../pogruzki/pogruzki';
import { CarSelectionPage } from '../car-selection/car-selection';
import { SearchAddressPage } from '../search-address/search-address';
import { NativeGeocoder, NativeGeocoderReverseResult } from '@ionic-native/native-geocoder';
import { Diagnostic } from '@ionic-native/diagnostic';
import { Geolocation } from '@ionic-native/geolocation';

import { DatePicker } from '@ionic-native/date-picker';
import { DatePipe } from '@angular/common';
import { AlertController } from 'ionic-angular';
import { PogruzkiService } from '../../../services/pogruzki';
import { AuthService } from '../../../services/auth';
import { NativeGeocoderResultModel } from '../../../models/pogruzki/geocoder';
import { LocationTracker } from '../../../providers/location-tracker';


@IonicPage()
@Component({
  selector: 'page-nayti',
  templateUrl: 'nayti.html',
})
export class NaytiPage {

  private pogruzkaForm: FormGroup;
  
    masks: any;
    otkudaRadius: number = 0;
    kudaRadius: number = 0;
    goDate: string;
    selectedCarId = null;
    fromLat: number=0;
    fromLng: number=0;
    fromCity: string = "";
    toLat: number=0;
    toLng: number=0;
    toCity: string = "";
    isFrom: boolean = false;
    date: number = 0;
    isLoading0: boolean = false;
    isLoading1: boolean = false;
    userLatLng: {lat: number, lng: number};

    geocoderRes: NativeGeocoderResultModel;

    constructor(public navCtrl: NavController,
                public navParams: NavParams,
                private datePicker: DatePicker,
                private datePipe: DatePipe,
                private formBuilder: FormBuilder,
                public alertCtrl: AlertController,
                private loadingCtrl: LoadingController,
                private pogruzkiS: PogruzkiService,
                private authS: AuthService,
                public geolocation: Geolocation,
                private modalCtrl: ModalController,
                private nativeGeocoder: NativeGeocoder,
                private locationTracker: LocationTracker,
                private diagnostic: Diagnostic) {
      
      this.initPogruzkaForm();
      
      if(this.authS.isUserLogedIn){
        this.pogruzkiS.getMyPoints(false).then((res)=>{
          if(this.pogruzkiS.myPogruzkiArray.length > 0){
                this.locationTracker.startTracking();
          }
        }, (error)=>{
        })
      }

      console.log("NAYTI PAGE");
  };
  


  onResetSearchForm(id: number){
      switch (id) {
        case 0:
            this.pogruzkaForm.controls["from"].reset();
            this.isFrom = false;
            this.fromCity = ""; this.fromLat = 0; this.fromLng = 0;         
          break;
        case 1:
            this.pogruzkaForm.controls["to"].reset();
            this.toCity = ""; this.toLat = 0; this.toLng = 0;
          break;
        case 2:
            this.pogruzkaForm.controls["date"].reset();
            this.goDate = "";
          break;
        case 3:
            this.pogruzkaForm.controls["car_type"].reset();
            this.selectedCarId = null; 
          break;       
        default:
          break;
      }
  }

    onClickFindMe(isFrom: number){
      if(this.checkGps()){

        console.log(this.checkGps());
        if(isFrom == 0){
          this.isLoading0 = true;
        }else{
          this.isLoading1 = true;
        }
        if(this.getUserLocation()){
          this.findMyLocationName(isFrom);
        }else{
          this.pogruzkiS.presentToast("Не можем определить ваше местоположение, провертье что GPS включено!", "error", "top", false, "", 3000);
        }
      }else{
        this.pogruzkiS.presentToast("Не можем определить ваше местоположение, провертье что GPS включено!", "error", "top", false, "", 3000);
      }
    }

    checkGps(){
      var isGpsEnabled = true;
      this.diagnostic.isLocationEnabled().then((isAvailable)=>{
        console.log("TRUE", isAvailable);
        alert(isAvailable);
        isGpsEnabled = true; 
      }).catch((error: any)=>{
        console.log("FALSE", error);
        isGpsEnabled = false; 
      });
      return isGpsEnabled;
    }

    enableGPS(){
      this.diagnostic.isGpsLocationEnabled().then((isAvailable)=>{
        if(!isAvailable){
          this.showGpsAlert(isAvailable);
        }
      }).catch((error: any)=>{
      });
    }


    showGpsAlert(res: any){
        const alert = this.alertCtrl.create({
        title: '<b>Геолокация</b>',
        message: 'Не можем определить ваше местоположение, включите GPS',
        buttons: [
          {
            text: 'Отмена',
            role: 'cancel',
            handler: () => {
              console.log('Cancel clicked');
            }
          },
          {
            text: 'Настройки',
            handler: () => {
              this.diagnostic.switchToLocationSettings();
            }
          }
        ]
      });
      alert.present();
    }


    findMyLocationName(isFrom: number){
      console.log("findMyLocationName()");   
      this.nativeGeocoder.reverseGeocode(this.userLatLng.lat, this.userLatLng.lng).then((result: NativeGeocoderReverseResult) => {
        this.geocoderRes = result;
        this.setVars(isFrom, this.geocoderRes.locality, this.userLatLng.lat, this.userLatLng.lng);
      }).catch((error: any) => {
        this.isLoading0 ? this.isLoading0 = false : this.isLoading1 = false;
        console.log(error);
        this.showAlert("error");
      });
    }
  

    showAlert(text: string) {
      let alert = this.alertCtrl.create({
        subTitle: text,
        buttons: ['OK']
      });
      alert.present();
    }


    onSearchAddressSelect(isFrom: number){
      const searchModal = this.modalCtrl.create(SearchAddressPage);
      searchModal.onDidDismiss((data: {city: string, lat: number, lng: number})=>{
        if(data){
          console.log(data)
          this.setVars(isFrom, data.city, data.lat, data.lng);
        }
      });
      searchModal.present();
    }


    setVars(isFrom: number, city: string, lat: number, lng: number){
      this.isLoading0 ? this.isLoading0 = false : this.isLoading1 = false;
      if(isFrom == 0){
        this.isFrom = true;
        this.pogruzkaForm.controls["from"].setValue(city);
        this.fromCity = city;
        this.fromLat = lat;
        this.fromLng = lng;
        console.log(this.pogruzkaForm.value);
      }else if(isFrom == 1){
        this.pogruzkaForm.controls["to"].setValue(city);
        this.toCity = city;
        this.toLat = lat;
        this.toLng = lng;
      }
    }


    onCarSelect(){
      const carSelectModal = this.modalCtrl.create(CarSelectionPage);
      carSelectModal.onDidDismiss(data => {
        if(data){
          this.selectedCarId = data.car.id;
          this.pogruzkaForm.controls["car_type"].setValue(data.car.name);
        }
      });
      carSelectModal.present();
    }
    

    onChange(ev: any, st: number){
      if(st == 0){
        this.otkudaRadius = ev.value;
      }else{
        this.kudaRadius = ev.value;
      }
    }

    
    onSelectDate(){
      this.datePicker.show({
        date: new Date(),
        mode: 'date',
        minDate: new Date(),
        cancelButtonLabel: "Отмена",
        androidTheme: this.datePicker.ANDROID_THEMES. THEME_DEVICE_DEFAULT_LIGHT
      }).then(
        date => {
          let dt = date.getTime();
          this.date = dt/1000;
          this.goDate = this.datePipe.transform(date, 'dd.MM.yyyy');
          this.pogruzkaForm.controls['date'].setValue(this.goDate);
        },err => {
          console.log('Error occurred while getting date: ', err);
        }
      );
    }
  

    onGetOrders(){
      this.pogruzkiS.isObratnyePogruzkiPage = false;
      console.log("form", this.pogruzkaForm.value);
      const loading = this.loadingCtrl.create();
      let creds = {
        "from_lat": this.fromLat,
        "from_lng": this.fromLng,
        "from_radius": this.otkudaRadius, //this.otkudaRadius
        "from_note": this.fromCity.toString(),
        "to_lat": this.toLat,
        "to_lng": this.toLng,
        "to_radius": this.kudaRadius,
        "to_note": this.toCity.toString(),
        "date": this.date,
        "type_auto": this.selectedCarId
      };
      console.log(creds);
      loading.present();
      this.pogruzkiS.onGetOrders(creds).then((res: any)=>{
        loading.dismiss();
        this.navCtrl.push(PogruzkiPage);
      }, (error: any)=>{
         loading.dismiss();
      })
    }


    initPogruzkaForm(){
      this.pogruzkaForm = this.formBuilder.group({
        from:         ['', [Validators.required]],
        from_radius:  [0],
        to:           ['', [Validators.required]],
        to_radius:    [0],
        date:         ['', [Validators.required]],
        car_type:     ['', [Validators.required]],
      });

      this.getUserLocation();

    }


    getUserLocation(){
      let isOk = true;
      this.geolocation.getCurrentPosition().then((resp) => {
        this.userLatLng = {
          lat: resp.coords.latitude,
          lng: resp.coords.longitude
        };
        isOk = true;
        this.locationTracker.lat = resp.coords.latitude;
        this.locationTracker.lng = resp.coords.longitude;
        console.log("GOT USER LOCATION",this.userLatLng);
      }).catch((error) => {
        console.log('Error getting location', error);
        isOk = false;
      });

      return isOk
    }


}
