import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { PogruzkaPage } from './pogruzka';

@NgModule({
  declarations: [
    PogruzkaPage,
  ],
  imports: [
    IonicPageModule.forChild(PogruzkaPage),
  ],
})
export class PogruzkaPageModule {}
