import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { ProfileService } from '../../services/profile';


@IonicPage()
@Component({
  selector: 'page-help',
  templateUrl: 'help.html',
})
export class HelpPage {

  
  constructor(public navCtrl: NavController,
              public navParams: NavParams,
              private profileS: ProfileService) {

      if(!this.profileS.questions){
        let gte =  JSON.parse(localStorage.getItem("gte"));
        this.profileS.getFaq(gte.token, true);
      }
  
  }


  trackByFn(index, item) {
    return item.id;
  }

}
