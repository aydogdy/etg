import { Component, ViewChild } from '@angular/core';
import { IonicPage, NavController, NavParams, ToastController, LoadingController, Events } from 'ionic-angular';
import { Validators, FormGroup, FormBuilder } from '@angular/forms';
import { AuthService } from '../../../services/auth';
import { NaytiPage } from '../../pogruzki/nayti/nayti';
import { PogruzkiService } from '../../../services/pogruzki';

@IonicPage()
@Component({
  selector: 'page-sms',
  templateUrl: 'sms.html',
})
export class SmsPage {

  @ViewChild('sms') sms:any;
  

  private smsForm: FormGroup;
  creds: {"phone": string, "inn": string};
  phone: String = "";
  title: string = "Подтверждение регистрации";
  msg: string = "";

  constructor(public navCtrl: NavController,
              public navParams: NavParams,
              private formBuilder: FormBuilder,
              public toastCtrl: ToastController,
              private loadingCtrl: LoadingController,
              private pogruzkiS: PogruzkiService,
              public events: Events,
              private authS: AuthService) {
  
    if(this.authS.isNewUser){
      this.creds = this.navParams.get("creds");
      this.msg = this.navParams.get("msg");
      console.log(this.creds);
    }else{
      this.phone = this.navParams.get("phone");
      this.msg = this.navParams.get("msg");
      this.title = "Подтверждение авторизации";
    }
                
    this.smsForm = this.formBuilder.group({
      sms:  ['', Validators.compose([Validators.minLength(4), Validators.required])]
    });
    console.log("SmsPage");
  }


  ionViewDidEnter() {
      setTimeout(() => {
          this.sms.setFocus();
      },100);
  }

  login(){
    this.authS.isNewUser ? this.newUserConfirm() : this.userConfirm();
  }

  newUserConfirm(){
      const loading = this.loadingCtrl.create();
      loading.present();

      let cred = {
        "code": this.smsForm.value.sms,
        "phone": this.creds.phone,
        "inn": this.creds.inn
      };

      this.authS.confirmRegistration(cred).then((res: any)=>{
        loading.dismiss();
        this.authS.isUserLogedIn = true;
        this.authS.presentToast(res.data.message, "success", 'top', false, '', 3000);
        
        if(this.authS.fromPogruzka){
          this.navCtrl.pop();
          this.authS.closeModal = true;
        }else{
          this.navCtrl.setRoot(NaytiPage);
          this.authS.closeModal = false;
        };
        this.events.publish('user:login');

        this.pogruzkiS.longPoll(0);
      },(error: any)=>{
        this.msg = error.data.message;
        loading.dismiss();
      });
  }

  userConfirm(){
      const loading = this.loadingCtrl.create();
      loading.present();
      
      let cred = {
        "phone": this.phone,
        "code": this.smsForm.value.sms,
        "onesignal_player_id": this.authS.userIdForPush
      };

      this.authS.confirmLogedUser(cred).then((res: any)=>{
        loading.dismiss();
        this.authS.isUserLogedIn = true;
        this.authS.presentToast(res.data.message, "success", 'top', false, '', 3000);

        if(this.authS.fromPogruzka){
          this.navCtrl.pop();
          this.authS.closeModal = true;
        }else{
          this.navCtrl.setRoot(NaytiPage);
          this.authS.closeModal = false;
        };
        this.events.publish('user:login');
        this.pogruzkiS.longPoll(0);  
      },(error: any)=>{
        this.msg = error.data.message;
        loading.dismiss();
      });
  }

}
