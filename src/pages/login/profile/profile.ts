import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, ModalController } from 'ionic-angular';
import { RegFormPage } from '../reg-form/reg-form';
import { ProfileService } from '../../../services/profile';

import { PhotoViewer } from '@ionic-native/photo-viewer';


@IonicPage()
@Component({
  selector: 'page-profile',
  templateUrl: 'profile.html',
})
export class ProfilePage {

  constructor(public navCtrl: NavController,
              private modalCtrl: ModalController,
              private profileS: ProfileService,
              private photoViewer: PhotoViewer,
              public navParams: NavParams) {

       if(!this.profileS.profileSavedData){
          let showLoader = true;
          this.profileS.getProfileData(showLoader); 
       }

  }

  ionViewWillEnter(){
      let newToken = JSON.parse(localStorage.getItem("gte"));
      if(newToken["token"] !== this.profileS.gte.token){
          let showLoader = true;
          this.profileS.getProfileData(showLoader);
          this.profileS.gte.token = newToken["token"];
      }
  }

  doRefresh(refresher) {
    let showLoader = false;
    this.profileS.getProfileData(showLoader).then(res=>{
      refresher.complete();
    })
  }
  
  onEdit(){
    const editModal = this.modalCtrl.create(RegFormPage, {"mode": "Edit"});
    editModal.onDidDismiss((data: any)=>{
      if(data){
        if(data.reload){
          this.profileS.getProfileData(true); 
        }
      }
    });
    editModal.present();
  }

  onViewPhoto(photo: string, title: string){
    console.log(photo);
    this.photoViewer.show(photo, title, {share: true});
  }

}
