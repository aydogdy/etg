import { Component } from '@angular/core';
import { NavController, NavParams, LoadingController, ToastController, ModalController, ViewController } from 'ionic-angular';
import { Validators, FormGroup, FormBuilder } from '@angular/forms';

import { AuthService } from '../../../services/auth';
import { RegCreds } from '../../../models/auth/regCreds';
import { AutoPage } from '../auto/auto';
import { SmsPage } from '../sms/sms';
import { HistoryService } from '../../../services/history';

@Component({
  selector: 'page-login',
  templateUrl: 'login.html'
})
export class LoginPage {

  public masks: any;
  private loginForm: FormGroup;
  private phoneForm: FormGroup;
  
  typeauto: number = null;
  title: string;
  creds: RegCreds;
  phone: string = "";
  isReadonly: boolean = false;


  constructor(public navCtrl: NavController,
              private formBuilder: FormBuilder,
              public toastCtrl: ToastController,
              private loadingCtrl: LoadingController,
              public navParams: NavParams,
              private viewCtrl: ViewController,
              private historyS: HistoryService,
              private modalCtrl: ModalController,
              private authS: AuthService) {

                
       this.authS.isNewUser = false;

        this.loginForm = this.formBuilder.group({
          phone: ['',  Validators.compose([Validators.minLength(12), Validators.required])],
          email: ['', [Validators.required]],
          nds:   [1, [Validators.required]],
          inn:   ['', [Validators.required]],
          name:  ['', [Validators.required]],
          code_ati: [''],
          type_auto: [null]
        });
        this.title = "Авторизация";
        
        this.phoneForm = this.formBuilder.group({
          phone:  ['', Validators.compose([Validators.minLength(12), Validators.required])]
        });


        this.masks = {
          phone: ['+', '7', /[1-9]/, /\d/, /\d/, /\d/, /\d/, /\d/, /\d/, /\d/, /\d/, /\d/]
        };
        
        console.log("LoginPage");
        
  }

  ionViewWillEnter(){
    if(this.authS.closeModal){
      this.onClose();
    }
    this.historyS.oneSignalConf();
  }
  
  show(){
    this.authS.isNewUser = !this.authS.isNewUser
    if(this.authS.isNewUser){
      this.loginForm.controls["phone"].setValue(this.phoneForm.controls["phone"].value);
      this.title = "Регистрация";
    }else{
      this.title = "Авторизация";
    }
  }


  onClose(){
    this.viewCtrl.dismiss();
  }


  onClickAuto(){
    const autoModal = this.modalCtrl.create(AutoPage, {"mode": "auto"});
    autoModal.onDidDismiss((data)=>{
      if(data){
        this.loginForm.controls['type_auto'].setValue(data.item.name);
        this.typeauto = data.item.id;
      }
    });
    autoModal.present();
  }


  onClickCompany(){
    const autoModal = this.modalCtrl.create(AutoPage, {"mode": "company"});
    
    autoModal.onDidDismiss((data)=>{
      if(data){
        if(data.item.inn && data.item.inn !== "null"){ // SELECTED company
          console.log(data.item);
          this.loginForm.controls['name'].setValue(data.item.name);
          this.loginForm.controls['inn'].setValue(data.item.inn);
          this.loginForm.controls['nds'].setValue(data.item.nds);
          this.loginForm.controls['code_ati'].setValue(data.item.code_ati);
          this.isReadonly = true;
        }else{ // NEW company
          console.log(data.item);
          this.loginForm.controls['inn'].setValue(data.item);
          this.loginForm.controls['name'].setValue("");
          this.loginForm.controls['nds'].setValue(1);
          this.loginForm.controls['code_ati'].setValue("");
          this.isReadonly = false;
        }
      }
    });

    autoModal.present();
  }


  onGetSms(){
      const loading = this.loadingCtrl.create();
      loading.present();

        this.creds = {
          "phone": this.loginForm.value.phone.substr(1),
          "email": this.loginForm.value.email,
          "nds": this.loginForm.value.nds,
          "inn": this.loginForm.value.inn,
          "name": this.loginForm.value.name,
          "code_ati": this.loginForm.value.code_ati,
          "type_auto": {
            "id": this.typeauto,
            "name": ""
          }
        };

        let cred = {
          "phone": this.creds.phone,
          "inn": this.creds.inn
        };

      this.authS.getSmsCode(this.creds).then((res: any)=>{
        loading.dismiss();
        console.log(res);
        this.navCtrl.push(SmsPage, {"creds": cred, "msg": res.data.message});
      },(error: any)=>{
        loading.dismiss();
        this.authS.presentToast(error.data.message, "error", 'top', false, '', 3000);
      });
  }


  registeredLogin(){
    let phone = this.phoneForm.value.phone.substr(1);
    console.log(phone);

    this.authS.registeredLogin(phone).then((res: any)=>{
      this.navCtrl.push(SmsPage, {"phone": phone, "msg": res.data.message});
    },(error: any)=>{
      console.log(error);
      this.authS.presentToast(error.data.message, "error", 'top', false, '', 3000);
    });
  }


  onClickContent(){
    if(this.loginForm.value.phone.length > 0 && this.loginForm.value.phone.length !== 12){
      let ph = this.loginForm.value.phone.slice(0, 12); // formating phone number like: 79656042605
      this.loginForm.controls['phone'].setValue(ph);
    }

    if(this.phoneForm.value.phone.length > 0 && this.phoneForm.value.phone.length !== 12){
      let ph = this.phoneForm.value.phone.slice(0, 12); // formating phone number like: 79656042605
      this.phoneForm.controls['phone'].setValue(ph);
    }
  }


}
