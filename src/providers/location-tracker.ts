import { Injectable, NgZone } from '@angular/core';
import { BackgroundGeolocation } from '@ionic-native/background-geolocation';
import { Geolocation } from '@ionic-native/geolocation';
import 'rxjs/add/operator/filter';
 

import { Platform } from 'ionic-angular';
import { PogruzkiService } from '../services/pogruzki';
import { AuthService } from '../services/auth';

@Injectable()
export class LocationTracker {
 
  public lat: number = 0;
  public lng: number = 0;
  public idBg: any;
  public idFg: any;

    constructor(public zone: NgZone,
              public geolocation: Geolocation,
              public backgroundGeolocation: BackgroundGeolocation,
              private authS: AuthService,
              private pogruzkiS: PogruzkiService,
              public platform: Platform ) {
            

            //Subscribe on pause
            this.platform.pause.subscribe(() => {
                this.getUserBackgroundPosition();
                if (this.idFg) {
                    clearInterval(this.idFg);
                }
            });
    
            //Subscribe on resume
            this.platform.resume.subscribe(() => {
                this.startTracking();
                if (this.idBg) {
                    clearInterval(this.idBg);
                }
                this.backgroundGeolocation.stop();
                this.backgroundGeolocation.finish();
            });
    
    }
 

    startTracking(){
        this.getUserCurrentPosition();
        // Turn ON the background-geolocation system.
        
        // Foreground Tracking
        this.idFg = setInterval(() => {
            // console.log("TIMEOUT: " + this.pogruzkiS.coordinatesInterval);
            let gte = JSON.parse(localStorage.getItem("gte"));
            if(gte && gte !== null){
                this.getUserCurrentPosition();
            }
        }, 10000);
    }

    stopTracking() {
        console.log("*****STOP stopTracking()*****");
        this.backgroundGeolocation.stop();
        this.backgroundGeolocation.finish();
        this.lat = null;
        this.lng = null;
        if (this.idBg) {
            clearInterval(this.idBg);
        }
        if (this.idFg) {
            clearInterval(this.idFg);
        }
        setTimeout(()=>{
            this.backgroundGeolocation.stop();
            if (this.idBg) {
                clearInterval(this.idBg);
            }
            if (this.idFg) {
                clearInterval(this.idFg);
            }
        }, 1000);
    }


    getUserBackgroundPosition(){
        console.log("*****START startTracking()*****");
        // Background Tracking
        let config = {
            desiredAccuracy: 0,
            stationaryRadius: 20,
            distanceFilter: 10,
            debug: false
        };
        
        this.backgroundGeolocation.configure(config).subscribe((location) => {
            console.log('BackgroundGeolocation:  ' + location.latitude + ',' + location.longitude);
            // Run update inside of Angular's zone
            this.zone.run(() => {
                this.lat = location.latitude;
                this.lng = location.longitude;
            });
            this.pogruzkiS.sendUserCoordinates(this.lat, this.lng);
            this.idBg = setInterval(() => {
                this.pogruzkiS.sendUserCoordinates(this.lat, this.lng);
            }, 10000);
            
            if(this.pogruzkiS.selectedPogruzka){
                if(this.pogruzkiS.selectedPogruzka.status.id < 4){
                    this.calculateUserDistance(this.pogruzkiS.selectedPogruzka.from.lat,this.pogruzkiS.selectedPogruzka.from.lng);
                }else{
                    this.calculateUserDistance(this.pogruzkiS.selectedPogruzka.to.lat,this.pogruzkiS.selectedPogruzka.to.lng);
                }
            }
            console.log("BACKGRAUND", this.lat, this.lng);
        }, (err) => {
            console.log(err);
        });

        this.backgroundGeolocation.start();
        
    }


    getUserCurrentPosition(){
        let options = {
            frequency: 3000,
            enableHighAccuracy: true
        };

        this.geolocation.getCurrentPosition(options).then((resp) => {
            this.zone.run(() => {
                this.lat = resp.coords.latitude;
                this.lng = resp.coords.longitude;
            });
            console.log("getUserCurrentPosition()");
            this.pogruzkiS.sendUserCoordinates(this.lat, this.lng);

            if(this.pogruzkiS.selectedPogruzka){
                if(this.pogruzkiS.selectedPogruzka.status.id < 4){
                    this.calculateUserDistance(this.pogruzkiS.selectedPogruzka.from.lat,this.pogruzkiS.selectedPogruzka.from.lng);
                }else{
                    this.calculateUserDistance(this.pogruzkiS.selectedPogruzka.to.lat,this.pogruzkiS.selectedPogruzka.to.lng);
                }
            }
        }).catch((error) => {
        });
    }


    calculateUserDistance(endLat: string, endLng: string){
        let start = [this.lat, this.lng]; // driver lat lng
        // let start = [55.8516702, 37.5757499];
        let end;
        let distance;
    
        if(endLat && endLng){
            end = [parseFloat(endLat), parseFloat(endLng)]; // Vygruska poit lat lng
            distance = this.distance(start, end);
        } 
        console.log(distance);
        if(distance && distance < 2000){ // 2 km
            this.pogruzkiS.isDriverNearPoint = true;
            console.log("NEAR POINT: " + distance + " m. - " + this.pogruzkiS.isDriverNearPoint);
        }else{
            this.pogruzkiS.isDriverNearPoint = false;
            console.log("NOT NEAR THE POINT: " + distance + " m. - " + this.pogruzkiS.isDriverNearPoint);
        }
    }
    
    
      distance(start: Array<number>, end: Array<number>){
        console.log("START",  start);
        console.log("END", end);
        
        if (start.length != 2) return false;
        if (end.length != 2) return false;
        
         //радиус Земли
        const R = 6372795;
        const M_PI = 3.14159265358979323846;
        
        //перевод коордитат в радианы
        var lat1 = start[0] * M_PI / 180;
        var lat2 = end[0] * M_PI / 180;
        var lon1 = start[1] * M_PI / 180;
        var lon2 = end[1] * M_PI / 180;
    
          //вычисление косинусов и синусов широт и разницы долгот
        var cl1 = Math.cos(lat1);
        var cl2 = Math.cos(lat2);
        var sl1 = Math.sin(lat1);
        var sl2 = Math.sin(lat2);
        var delta = lon2 - lon1;
        var cdelta = Math.cos(delta);
        var sdelta = Math.sin(delta);
        //вычисления длины большого круга
        var y = Math.sqrt(Math.pow(cl2*sdelta, 2) + Math.pow(cl1*sl2 - sl1*cl2*cdelta, 2));
        var x = sl1*sl2 + cl1*cl2*cdelta;
        var ad = Math.atan2(y, x);
        var dist = Math.round(ad*R); //расстояние между двумя координатами в метрах
        return dist;
      }

 
}