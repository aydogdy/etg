import { From } from "./from";
import { To } from "./to";
import { Status } from "./status";

export class Pogruzka {
    id: number;
    from: From;
    to: To;
    date: number;
    summary: number;
    type_auto: number;
    client: string;
    cargo: string;
    comment: string;
    offer_percent: string; 
    offer_price: string;
    number: number;
    status: Status;
    load_address: string;
    load_contact: string;
    load_contact_phone: string;
    unload_address: string;
    unload_contact: string;
    unload_contact_phone: string;
    
	constructor (id: number,
                from: From,
                to: To,
                date: number,
                summary: number,
                type_auto: number,
                client: string,
                cargo: string,
                comment: string,
                offer_percent: string,
                offer_price: string,
                number: number,
                status: Status,
                load_address: string,
                load_contact: string,
                load_contact_phone: string,
                unload_address: string,
                unload_contact: string,
                unload_contact_phone: string){

            this.id = id;
            this.from = from;
            this.to = to;
            this.date = date;
            this.summary = summary;
            this.type_auto = type_auto;
            this.client = client;
            this.cargo = cargo;
            this.comment = comment;
            this.offer_percent = offer_percent;
            this.offer_price = offer_price;
            this.number = number;
            this.status = status;
            this.load_address = load_address;
            this.load_contact = load_contact;
            this.load_contact_phone = load_contact_phone;
            this.unload_address = unload_address;
            this.unload_contact = unload_contact;
            this.unload_contact_phone = unload_contact_phone;
	}
}