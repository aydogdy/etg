export class From {
    
    note: string;
    lat: string;
    lng: string;
    address: string;
    contact: string;
    contact_phone: string;

	constructor (note: string,
                 lat: string,
                 lng: string,
                 address: string,
                 contact: string,
                 contact_phone: string){
    
            this.note = note;
            this.lat = lat;
            this.lng = lng;
            this.address = address;
            this.contact = contact;
            this.contact_phone = contact_phone;
    }
    
}