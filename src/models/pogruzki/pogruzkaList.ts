export class PogruzkaList {
    id: number;
    from_note: string;
    to_note: string;
    date: number;
    summary: number;
    offer: number;
    offer_company: number;
    offer_price: number;
    number: number;
    load_address: string;
    load_contact: string;
    load_contact_phone: string;
    unload_address: string;
    unload_contact: string;
    unload_contact_phone: string;

	constructor (id: number,
                 from_note: string,
                 to_note: string,
                 date: number,
                 summary: number,
                 offer: number,
                 offer_company: number,
                 offer_price: number,
                 number: number,
                 load_address: string,
                 load_contact: string,
                 load_contact_phone: string,
                 unload_address: string,
                 unload_contact: string,
                 unload_contact_phone: string){

            this.id = id;
            this.from_note = from_note;
            this.to_note = to_note;
            this.date = date;
            this.summary = summary;
            this.offer = offer;
            this.offer_company = offer_company;
            this.offer_price = offer_price;
            this.number = number;
            this.load_address = load_address;
            this.load_contact = load_contact;
            this.load_contact_phone = load_contact_phone;
            this.unload_address = unload_address;
            this.unload_contact = unload_contact;
            this.unload_contact_phone = unload_contact_phone;
        
	}
}