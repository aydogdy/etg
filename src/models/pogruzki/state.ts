export class State{
    
        id: number;
        state: string;
        date: number;
    
        constructor(id: number, state: string, date: number){
            this.id = id;
            this.state = state;
            this.date = date;
        }
    
    }