import { Auto } from "./autoType";

export class RegCreds {
	phone: string;
	email: string;
    nds: number;
    inn: string;
    name: string;
    code_ati: string;
    type_auto: Auto;

	constructor (phone: string,
                 email: string,
                 nds: number,
                 inn: string,
                 name: string,
                 code_ati: string,
                 type_auto: Auto){

		this.phone = phone;
        this.email = email;
        this.nds = nds;
        this.inn = inn;
        this.name = name;
        this.code_ati = code_ati;
        this.type_auto = type_auto;
        
	}
}