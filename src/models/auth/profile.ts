import { Auto } from "./autoType";

export class Profile {
    phone: string;
    email: string;
    inn: string;
    nds: number;
    name: string;
    code_ati: string;
    photo_passport: string;
    photo_vu: string;
    photo_sts: string;
    type_auto: Auto;
    vin: string;


	constructor (phone: string,
                 email: string,
                 inn: string,
                 nds: number,
                 name: string,
                 code_ati: string,
                 photo_passport: string,
                 photo_vu: string,
                 photo_sts: string,
                 type_auto: Auto,
                 vin: string){

        this.phone = phone;
        this.email = email;
        this.inn = inn;
        this.nds = nds;
        this.name = name;
        this.code_ati = code_ati;
        this.photo_passport = photo_passport;
        this.photo_vu = photo_vu;
        this.photo_sts = photo_sts;
        this.type_auto = type_auto;
        this.vin = vin;
    }
    
    
}