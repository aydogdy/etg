export class Company {
    
    id: number;
	name: string;
    inn: string;
    nds: number;
    code_ati: string;

	constructor (id: number,
                 name: string,
                 inn: string,
                 nds: number,
                 code_ati: string){

		this.id = id;
        this.name = name;
        this.inn = inn;
        this.nds = nds;
        this.code_ati = code_ati;
        
	}
}